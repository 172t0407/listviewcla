package com.example.listas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var frutas : ArrayList<String> = ArrayList()
        frutas.add("Limon")
        frutas.add("Manzana")
        frutas.add("Naranja")
        frutas.add("Uvas")
        frutas.add("Kiwis")

        val lista = findViewById<ListView>(R.id.lista)

        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,frutas)

        lista.adapter= adaptador
        lista.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->

            if (position == 0){

                var intent = Intent(getApplicationContext(), LimonActivity::class.java)
                startActivity(intent)
            }
            if (position == 1){

                var intent = Intent(getApplicationContext(), AppleActivity::class.java)
                startActivity(intent)
            }
            if (position == 2){

                var intent = Intent(getApplicationContext(), NaranjaActivity::class.java)
                startActivity(intent)
            }
            if (position == 3){

                var intent = Intent(getApplicationContext(), UvasActivity::class.java)
                startActivity(intent)
            }
            if (position == 4){

                var intent = Intent(getApplicationContext(), KiwisActivity::class.java)
                startActivity(intent)
            }
        }
    }
}